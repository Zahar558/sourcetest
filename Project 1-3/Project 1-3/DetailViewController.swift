//
//  DetailViewController.swift
//  Project 1-3
//
//  Created by Zakhar Sidorov  on 11/4/20.
//  Copyright © 2020 Zakhar Sidorov . All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    var selectedImage: String? 

    @IBOutlet var imageViewFullFlag: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let select = selectedImage {
        imageViewFullFlag.image = UIImage(named: select)
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(showName))
        }
    }
       
    @objc func showName() {
            let vc = UIActivityViewController(activityItems: ["\(selectedImage ?? "")"], applicationActivities: [])
         
            present(vc, animated: true)
            
        }

    
    }

