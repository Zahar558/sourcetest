//
//  TableViewController.swift
//  Project 1-3
//
//  Created by Zakhar Sidorov  on 11/4/20.
//  Copyright © 2020 Zakhar Sidorov . All rights reserved.
//

import UIKit

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    
    var flags = [String]()
    
    @IBOutlet var tablewView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tablewView.delegate = self
        tablewView.dataSource = self
        let fm = FileManager.default
            guard let puth = Bundle.main.resourcePath else {return}
            let items = try! fm.contentsOfDirectory(atPath: puth)
            for item in items {
                if item.hasSuffix("png") {
                    flags.append(item)
                }
                flags.shuffle()
                

        }
        print(flags)
        }
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return flags.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Country") as! TableViewCell
    let image = flags[indexPath.row]
    cell.imageFlag.image = UIImage(named: image)
        // MARK: - Table view data source

    return cell
}
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let detailVC = storyboard.instantiateViewController(identifier: "DetailViewController") as! DetailViewController
        detailVC.selectedImage = flags[indexPath.row]
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
    
}
