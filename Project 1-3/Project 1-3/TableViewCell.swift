

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet var imageFlag: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageFlag.layer.borderWidth = 1
        imageFlag.layer.borderColor = UIColor.black.cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
